//
//  Product.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct Product: Identifiable {
    var id = UUID()
    var type: ProductType
    var name: String
    var make: String
    var description: String
    var imageName: String { make }
    
    enum ProductType {
        case watch, orange
    }
}

#if DEBUG
let testData: [Product] = [
    .init(type: .orange, name: "Sweet orange", make: "Citrus × sinensis", description: "The orange fruit is an important agricultural product, used for both the juicy fruit pulp and the aromatic peel (rind). Orange blossoms (the flowers) are used in several different ways, as are the leaves and wood of the tree."),
    .init(type: .orange, name: "Mandarin orange", make: "Citrus reticulata", description: "A small citrus tree with fruit resembling other oranges, usually eaten plain or in fruit salads.[1] The tangerine is a group of orange-coloured citrus fruit consisting of hybrids of mandarin orange."),
    .init(type: .watch, name: "Rolex", make: "Sky-Dweller", description: "The Oyster Perpetual Sky-Dweller is a timepiece with a unique design that blends technological sophistication and ease of use."),
    .init(type: .watch, name: "Rolex", make: "Cosmograph Daytona", description: "The Oyster Perpetual Cosmograph Daytona is the ultimate tool watch for those with a passion for driving and speed.")
]
#endif
