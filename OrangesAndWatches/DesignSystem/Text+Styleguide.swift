//
//  Styleguide+FontStyles.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

extension Text {
    func appHeader() -> some View {
        self.modifier(AppHeaderModifier())
    }
    
    func appSubtitle() -> some View {
        self.modifier(AppSubtitleModifier())
    }
    
    func appOrangeButton() -> some View {
        self.modifier(AppOrangeButtonModifier())
    }
}

struct AppHeaderModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.appHeader)
            .foregroundColor(.black)
    }
}

struct AppSubtitleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.appSubtitle)
            .foregroundColor(.subtitleGrey)
    }
}

struct AppOrangeButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.appHeader)
            .foregroundColor(.white)
    }
}

//MARK: - Preview
struct Styleguide_FontStyles: View {
    var body: some View {
        VStack {
            Text("AppHeader")
                .appHeader()
            Text("AppSubtitle")
                .appSubtitle()
            Text("OrangeButtonLabel")
                .appOrangeButton()
                .background(Color.veryOrange)
        }
    }
}

struct Styleguide_FontStyles_Previews: PreviewProvider {
    static var previews: some View {
        Styleguide_FontStyles()
    }
}






