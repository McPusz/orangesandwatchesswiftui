//
//  App+Fonts.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

extension Font {
    static var appHeader: Font {
       .custom("Avenir-Black", size: 20)
    }
    
    static var appSubtitle: Font {
        .custom("Avenir-Medium", size: 13)
    }
}
