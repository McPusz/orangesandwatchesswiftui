//
//  BasicCell.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 07/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct BasicCell: View {
    
    let topLabel: String
    let bottomLabel: String
    let leftImage: Image
    
    var body: some View {
        HStack {
            leftImage
                .resizable()
                .frame(width: 65, height: 65)
            VStack(alignment: .leading) {
                Text(topLabel)
                    .appHeader()
                Text(bottomLabel)
                    .appSubtitle()
            }
        }
    }
}

struct BasicCell_Previews: PreviewProvider {
    static var previews: some View {
        BasicCell(topLabel: "Orange", bottomLabel: "very tasty", leftImage: Image(testData.first!.imageName))
    }
}

