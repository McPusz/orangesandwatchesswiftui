//
//  Styleguide+Colors.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI


extension Color {
    @nonobjc static var veryOrange: Color {
        Color(red: 237/255, green: 120/255, blue: 7/255)
    }
    
    @nonobjc static var subtitleGrey: Color {
        Color(red: 95/255, green: 93/255, blue: 93/255)
    }
    
    @nonobjc static var lightGrey: Color {
        Color(red: 216/255, green: 216/255, blue: 216/255)
    }
}
