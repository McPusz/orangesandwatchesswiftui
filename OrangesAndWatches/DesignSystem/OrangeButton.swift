//
//  OrangeButton.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 07/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct OrangeButton: View {
    let text: String
    let action: () -> ()
    private let height: CGFloat = 47
    
    var body: some View {
        Button(action: action) {
            Text(text)
                .appOrangeButton()
        }
        .frame(height: height)
        .frame(minWidth: 0, maxWidth: .infinity)
        .background(Color.veryOrange)
        .cornerRadius((height)/2)
        .padding(.horizontal, 18)
    }
}

struct OrangeButton_Previews: PreviewProvider {
    static var previews: some View {
        OrangeButton(text: "oranges", action: { print("hello") })
    }
}
