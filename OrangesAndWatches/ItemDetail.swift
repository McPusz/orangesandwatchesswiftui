//
//  ItemDetail.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct ItemDetail: View {
    let product: Product
   
    var body: some View {
        VStack(spacing: 3) {
            Image(product.imageName)
                .resizable()
                .frame(width: 200, height: 200)
            //TODO: use design system main header for name
            Text(product.name)
                .font(.system(size: 8, weight: .bold))
                .foregroundColor(.blue)
            //TODO: use design system main subtitle for name
            Text(product.make)
                .font(.system(size: 20, weight: .regular))
                .foregroundColor(.green)
            Text(product.description)
                .font(.system(size: 10, weight: .regular))
                .foregroundColor(.pink)
                .padding()
            Spacer()
            ZStack() {
                OrangeButton(text: "orange?", action: {})
            }
            .frame(width: 200, height: 90)
            .background(Color.lightGrey)
        }
    }
}

struct ItemDetail_Previews: PreviewProvider {
    static var previews: some View {
        ItemDetail(product: testData.first!)
    }
}
