//
//  ItemRow.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct ItemRow: View {
    let product: Product
    
    var body: some View {
        NavigationLink(destination: ItemDetail(product: product)) {
            HStack {
                Image(product.imageName)
                    .resizable()
                    .frame(width: 65, height: 65)
                VStack(alignment: .leading) {
                    //TODO: use design system main header for name
                    Text(product.name)
                        .font(.system(size: 10, weight: .bold))
                    //TODO: use design system main subtitle for name
                    Text(product.make)
                        .font(.custom("Papyrus", size: 20))
                        .foregroundColor(.gray)
                }
            }
        }
    }

}

struct ItemRow_Previews: PreviewProvider {
    static var previews: some View {
        ItemRow(product: testData.first!)
    }
}



















//    var body: some View {
//        NavigationLink(destination: ItemDetail(product: product)) {
//            BasicCell(topLabel: product.name,
//                      bottomLabel: product.make,
//                      leftImage: Image(product.imageName))
//        }
//    }
