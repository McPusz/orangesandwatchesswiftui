//
//  ContentView.swift
//  OrangesAndWatches
//
//  Created by Magdalena on 03/05/2020.
//  Copyright © 2020 McPusz. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var products = testData
    
    var body: some View {
        NavigationView {
            VStack {
                List(products) { product in
                    ItemRow(product: product)
                }.navigationBarTitle(Text("Products"))
                OrangeButton(text: "do you want?", action: {})
            }
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(products: testData)
    }
}
#endif
